FROM openjdk:11-jre-slim

COPY target/auth-service-0.0.1-SNAPSHOT.jar /auth-service.jar

EXPOSE 8000

ENTRYPOINT ["java", "-jar", "/auth-service.jar"]
