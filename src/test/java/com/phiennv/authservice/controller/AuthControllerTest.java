package com.phiennv.authservice.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.phiennv.authservice.domain.request.AuthRequest;
import com.phiennv.authservice.domain.response.AuthResponse;
import com.phiennv.authservice.service.UserService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(AuthControllerTest.class)
public class AuthControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    UserService userService;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void testApiGetProduct2xx() throws Exception {
        AuthRequest authRequest = new AuthRequest();
        authRequest.setUsername("phiennv");
        authRequest.setPassword("123456");

        String accessToken = "12312qhjg4hj234g248";
        String refreshToken = "234jgjhw45434g7";
        AuthResponse authResponse = new AuthResponse(accessToken, refreshToken);
        Mockito.when(userService.authenticate(authRequest)).thenReturn(authResponse);
        String requestBody = objectMapper.writeValueAsString(authRequest);
        mockMvc.perform(post("/api-auth/sign-in")
                        .contentType(MediaType.APPLICATION_JSON).content(requestBody))
                .andExpect(status().is4xxClientError())
                .andDo(print());
    }

}
