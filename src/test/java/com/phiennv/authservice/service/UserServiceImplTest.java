package com.phiennv.authservice.service;

import com.phiennv.authservice.domain.model.User;
import com.phiennv.authservice.domain.request.AuthRequest;
import com.phiennv.authservice.domain.response.AuthResponse;
import com.phiennv.authservice.repository.UserRepository;
import com.phiennv.authservice.service.impl.UserServiceImpl;
import com.phiennv.authservice.utils.JWTUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTest {

    @InjectMocks
    UserServiceImpl userService;

    @Mock
    UserRepository userRepository;

    @Mock
    PasswordEncoder passwordEncoder;

    @Mock
    private JWTUtils jwtUtils;

    @Test
    public void authentication() {
        User user = new User();
        user.setId(1l);
        user.setUsername("phiennv");
        user.setRole("ROLE_USER");
        user.setEmail("ngovanphien@gmail.com");
        user.setFirstName("ngo");
        user.setLastName("phien");
        user.setNumberPhone("0865546273");
        user.setActive(true);
        user.setNotLocked(true);
        user.setPassword("$2a$10$4lzFm90lIlNuePG4Fza.yOBe3ByC.AxG6YIeO3FIaZeVRk42EWpli");
        AuthRequest authRequest = new AuthRequest();
        authRequest.setUsername("phiennv");
        authRequest.setPassword("123456");
        Mockito.lenient().when(passwordEncoder.matches(Mockito.anyString(), Mockito.anyString())).thenReturn(true);
        Mockito.when(userRepository.findUserByUsername("phiennv")).thenReturn(user);
        AuthResponse authResponse = userService.authenticate(authRequest);

        assertEquals(authResponse.getAccessToken(), jwtUtils.generateAccessToken(user));
    }
}
