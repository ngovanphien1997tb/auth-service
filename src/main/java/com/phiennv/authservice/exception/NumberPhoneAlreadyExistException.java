package com.phiennv.authservice.exception;

public class NumberPhoneAlreadyExistException extends RuntimeException{
    public NumberPhoneAlreadyExistException(String message) {
        super(message);
    }
}
