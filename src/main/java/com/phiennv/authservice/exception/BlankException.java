package com.phiennv.authservice.exception;

public class BlankException extends RuntimeException{

    private String errCode;

    private String errMessage;
    public BlankException(String errCode, String errMessage) {
        this.errCode = errCode;
        this.errMessage = errMessage;
    }
}
