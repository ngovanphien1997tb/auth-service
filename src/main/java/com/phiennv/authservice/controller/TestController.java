package com.phiennv.authservice.controller;

import com.phiennv.authservice.exception.UsernameAlreadyExistsException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
    @GetMapping("/test")
    public String test() {
        return "Hello";
//        throw new UsernameAlreadyExistsException("validation.username.already.exist.message");
    }
}
