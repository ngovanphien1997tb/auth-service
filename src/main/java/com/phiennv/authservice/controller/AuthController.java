package com.phiennv.authservice.controller;

import com.phiennv.authservice.constain.ConstantGlobal;
import com.phiennv.authservice.domain.dto.ApiMessage;
import com.phiennv.authservice.domain.dto.ApiResponse;
import com.phiennv.authservice.domain.request.AuthRequest;
import com.phiennv.authservice.domain.request.RefreshAccessTokenRequest;
import com.phiennv.authservice.domain.request.RegisterForm;
import com.phiennv.authservice.domain.response.AuthResponse;
import com.phiennv.authservice.service.UserService;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;


@RestController
public class AuthController {

    @Autowired
    private UserService userService;
    @PostMapping("/sign-in")
    public ResponseEntity<ApiResponse> signIn(@Valid @RequestBody AuthRequest authRequest) {
        AuthResponse authResponse = userService.authenticate(authRequest);
        return ResponseEntity.ok().body(
                ApiResponse.createResponse(HttpStatus.OK,
                        Collections.singletonList(ApiMessage.builder().code(ConstantGlobal.MESSAGE_CODE.SUCCESS).build())
                ,authResponse)
        );
    }

    @PostMapping("/register")
    public ResponseEntity<ApiResponse> register(@Valid @RequestBody RegisterForm form) {
        userService.register(form);
        return ResponseEntity.ok(ApiResponse
                .createResponse(HttpStatus.OK
                        , Collections.singletonList(ApiMessage.builder().code(ConstantGlobal.MESSAGE_CODE.SUCCESS).content(null).build())
                        , null));
    }

    @PostMapping("/refresh-token")
    public ResponseEntity<ApiResponse> refreshAccessToken(@Valid @RequestBody RefreshAccessTokenRequest refreshToken) {
        AuthResponse authResponse = userService.refreshAccessToken(refreshToken.getRefreshToken());
        return ResponseEntity.ok().body(
                ApiResponse.createResponse(HttpStatus.OK,
                        Collections.singletonList(ApiMessage.builder().code(ConstantGlobal.MESSAGE_CODE.SUCCESS).build())
                        ,authResponse)
        );
    }
}
