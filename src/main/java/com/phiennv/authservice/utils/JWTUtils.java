package com.phiennv.authservice.utils;

import com.phiennv.authservice.domain.model.User;
import com.phiennv.authservice.exception.AuthenticateException;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class JWTUtils {

    @Value("${jwt.secretKey.accessToken}")
    private String secretKeyAccessToken;

    @Value("${jwt.expiration.accessToken}")
    private int expirationAccessToken;// seconds

    @Value("${jwt.secretKey.refreshToken}")
    private String secretKeyRefreshToken;

    @Value("${jwt.expiration.refreshToken}")
    private int expirationRefreshToken;// days

    public String generateAccessToken(User user){
        long now = System.currentTimeMillis();
        String accessToken = Jwts.builder()
                .setSubject(user.getUsername())
                .setIssuedAt(new Date(now))
                .setExpiration(new Date(now + expirationAccessToken*1000))
                .claim("username", user.getUsername())
                .claim("email", user.getEmail())
                .claim("numberPhone", user.getNumberPhone())
                .claim("firstName", user.getFirstName())
                .claim("lastName", user.getLastName())
                .claim("role", user.getRole())
                .signWith(SignatureAlgorithm.HS512, secretKeyAccessToken)
                .compact();
        return accessToken;
    }

    public String generateRefreshToken(User user) {
        long now = System.currentTimeMillis();
        String refreshToken = Jwts.builder()
                .setSubject(user.getUsername())
                .setIssuedAt(new Date(now))
                .setExpiration(new Date(now + expirationRefreshToken*24*60*60*1000))
                .signWith(SignatureAlgorithm.HS512, secretKeyRefreshToken)
                .compact();
        return refreshToken;
    }

    public String validateRefreshToken(String refreshToken) {
        Jws<Claims> claimsJws = Jwts.parser()
                .setSigningKey(secretKeyRefreshToken)
                .parseClaimsJws(refreshToken);

        Claims claims = claimsJws.getBody();
        Date expiration = claims.getExpiration();

        validateRefreshTokenExpiration(expiration);
        String username = claims.getSubject();
        return username;
    }

    public boolean validateRefreshTokenExpiration(Date expiration) {
        if(expiration.getTime() > System.currentTimeMillis()) {
            return true;
        }
        throw new AuthenticateException("jwt.expiration.error.message");
    }
}
