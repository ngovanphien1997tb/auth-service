package com.phiennv.authservice.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.util.Locale;
import java.util.ResourceBundle;

@Slf4j
public class MessageUtils {
    public static String getMessage(String label, String lang) {
        try {
            ResourceBundle bundle = ResourceBundle.getBundle("message", Locale.forLanguageTag(lang));
            String errorInBundle = bundle.getString(label);

            if (StringUtils.isNotBlank(errorInBundle)) {
                return errorInBundle;
            }
        } catch (Exception ex) {
            log.warn("Unable to locate resource. code: " + label);
        }

        return label;
    }
}
