package com.phiennv.authservice.service;

import com.phiennv.authservice.domain.request.AuthRequest;
import com.phiennv.authservice.domain.request.RegisterForm;
import com.phiennv.authservice.domain.response.AuthResponse;

public interface UserService {

    AuthResponse authenticate(AuthRequest authRequest);

    AuthResponse refreshAccessToken(String refreshToken);
    void register(RegisterForm registerForm);
}
