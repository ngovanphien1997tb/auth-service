package com.phiennv.authservice.service.impl;


import com.phiennv.authservice.constain.ConstantGlobal;
import com.phiennv.authservice.domain.model.User;
import com.phiennv.authservice.domain.request.AuthRequest;
import com.phiennv.authservice.domain.request.RegisterForm;
import com.phiennv.authservice.domain.response.AuthResponse;
import com.phiennv.authservice.exception.AuthenticateException;
import com.phiennv.authservice.exception.EmailAlreadyExistsException;
import com.phiennv.authservice.exception.NumberPhoneAlreadyExistException;
import com.phiennv.authservice.exception.UsernameAlreadyExistsException;
import com.phiennv.authservice.repository.UserRepository;
import com.phiennv.authservice.service.UserService;
import com.phiennv.authservice.utils.JWTUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private JWTUtils jwtUtils;

    @Override
    public AuthResponse refreshAccessToken(String refreshToken) {
        String username = jwtUtils.validateRefreshToken(refreshToken);
        User user = userRepository.findUserByUsername(username);
        return new AuthResponse(jwtUtils.generateAccessToken(user), refreshToken);
    }

    @Override
    public AuthResponse authenticate(AuthRequest authRequest) {
        User user = validateUserInfo(authRequest);
        return new AuthResponse(jwtUtils.generateAccessToken(user), jwtUtils.generateRefreshToken(user));
    }

    private User validateUserInfo(AuthRequest authRequest) {
        User user = userRepository.findUserByUsername(authRequest.getUsername());
        if(user == null) {
            throw new UsernameNotFoundException("validation.user.notFound.error.message");
        } else {
            if(user.getActive() == false || user.getNotLocked() == false) {
                throw new AuthenticateException("account.error.message");
            }
            if(passwordEncoder.matches(authRequest.getPassword(), user.getPassword())) {
                return user;
            }
        }

        throw new AuthenticateException("authenticate.error.message");

    }

    @Override
    public void register(RegisterForm registerForm) {
        checkUsernameAlreadyExists(registerForm.getUsername());
        checkEmailAlreadyExists(registerForm.getEmail());
        checkNumberPhoneAlreadyExists(registerForm.getNumberPhone());
        String passwordEncode = passwordEncoder.encode(registerForm.getPassword());
        User user = User.builder().username(registerForm.getUsername())
                .firstName(registerForm.getFirstName())
                .lastName(registerForm.getLastName())
                .password(passwordEncode)
                .email(registerForm.getEmail())
                .numberPhone(registerForm.getNumberPhone())
                .role(ConstantGlobal.ROLE.USER_ROLE)
                .isActive(true)
                .isNotLocked(true)
                .build();
        userRepository.save(user);
    }

    public boolean checkUsernameAlreadyExists(String username) {
        User user = userRepository.findUserByUsername(username);
        if(user == null) {
            return false;
        } else {
            throw new UsernameAlreadyExistsException("validation.username.already.exist.message");
        }
    }

    public boolean checkEmailAlreadyExists(String email) {
        User user = userRepository.findUserByEmail(email);
        if (user == null) {
            return false;
        } else {
            throw new EmailAlreadyExistsException("validation.email.already.exist.message");
        }
    }

    public boolean checkNumberPhoneAlreadyExists(String numberPhone) {
        User user = userRepository.findUserByNumberPhone(numberPhone);
        if (user == null) {
            return false;
        } else {
            throw new NumberPhoneAlreadyExistException("validation.numberPhone.already.exist.message");
        }
    }
}
