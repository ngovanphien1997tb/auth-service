package com.phiennv.authservice.domain.request;

import javax.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class RefreshAccessTokenRequest {
    @NotBlank(message = "validation.refreshToken.notBlank.error.message")
    private String refreshToken;
}
