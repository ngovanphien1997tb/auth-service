package com.phiennv.authservice.domain.request;

import javax.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class AuthRequest {
    @NotBlank(message = "validation.username.notnull.message")
    private String username;

    @NotBlank(message = "validation.username.notnull.message")
    private String password;
}
