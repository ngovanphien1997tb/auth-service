package com.phiennv.authservice.domain.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import lombok.Data;

import java.time.LocalDate;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RegisterForm {

    private String firstName;

    private String lastName;

    @NotBlank(message = "validation.username.notnull.message")
    private String username;

    @NotBlank(message = "validation.password.notnull.message")
    @Pattern(regexp = "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$"
    , message = "validation.password.constant.message")
    private String password;

    @NotBlank(message = "validation.email.notnull.message")
    @Pattern(regexp = "^[A-Za-z0-9+_.-]+@[A-Za-z0-9.-]+$"
            , message = "validation.email.format.error.message")
    private String email;

    @JsonFormat(pattern = "dd/MM/yyyy")
    private LocalDate dob;

    private String address;

    @NotBlank(message = "validation.numberPhone.notnull.message")
    @Pattern(regexp = "^\\d{10}$", message = "validation.numberPhone.format.error.message")
    private String numberPhone;
}
